﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
namespace LogInTeletinPOCU
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. Open URL
            IWebDriver driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Url = "https://www.techlistic.com/p/selenium-practice-form.html";
            driver.Manage().Window.Maximize();




            //2. Enter First Name and Enter Last Name
            driver.FindElement(By.Name("firstname")).SendKeys("Teletin");
            driver.FindElement(By.Name("lastname")).SendKeys("Alexandra-Ionela");




            //3. Select gender (Female)
            driver.FindElement(By.Id("sex-1")).Click();




            //4.  Select years of experience
            driver.FindElement(By.Id("exp-1")).Click();




            //5. Enter date
            driver.FindElement(By.Id("datepicker")).SendKeys("2019-2021");

           


            //8. Select continent
            driver.FindElement(By.Id("continents")).SendKeys("Europe");






            //9. Select multiple commands from a multi select box (Nu pot selecta mai multe comenzi, nici pe site nu pot )
            driver.FindElement(By.Id("selenium_commands")).SendKeys("Browser Commands");
            // driver.FindElement(By.Id("selenium_commands")).SendKeys("WebElement Commands");

            //driver.FindElement(By.ClassName("input-file")).SendKeys("7a5e72e46eef38079c9a0494d34b41bb");

            //driver.FindElement(By.Id("photo")).Click();
            //var reportDownloadButton = driver.FindElement(By.XPath("//*[@id='post-body-3077692503353518311']/div/div/div[29]/div[2]/div/a"));
            //reportDownloadButton.Click();
            //driver.FindElement(By.CssSelector("#post-body-3077692503353518311 > div > div > div.controls > div:nth-child(2) > div > a")).Click();
            //driver.FindElement(By.XPath("//*[@id='post - body - 3077692503353518311']")).Click();

            //driver.FindElement(By.Id("photo")).Click();
            
            //driver.FindElement(By.LinkText("Button")).Click();





            //7. Select automation tools you are familiar with (multiple checkboxes)
            IWebElement oCheckBox = driver.FindElement(By.CssSelector("input[value='Selenium IDE']"));
            oCheckBox.Click();
            IWebElement oCheckBox2 = driver.FindElement(By.CssSelector("input[value='Selenium Webdriver']"));
            oCheckBox2.Click();
// driver.FindElement(By.XPath("//*[@id='tool-0']")).Click();
            //driver.FindElement(By.Id("tool-0")).Click();
            //driver.FindElement(By.CssSelector("#tool-0"));
            //ChromeOptions options = new ChromeOptions();
            
            
            
            
            
            
            //6. Select profession

            IWebElement profession = driver.FindElement(By.CssSelector("input[value='Automation Tester']"));
            profession.Click();
            //IList<IWebElement> prof_list = driver.FindElements(By.Name("profession"));
            // This will tell you the number of checkboxes are present

            //int iSize = prof_list.Count;

            // Start the loop from first checkbox to last checkboxe
            //for (int i = 1; i < iSize; i++)
            // {
            // Store the checkbox name to the string variable, using 'Value' attribute
            //  String Value = prof_list.ElementAt(i).GetAttribute("value");

            // Select the checkbox it the value of the checkbox is same what you are looking for
            // if (Value.Equals("Automation Tester"))
            // {
            //chkBx_Profession.ElementAt(i).Click();
            // This will take the execution out of for loop
            //  break;
            //}
            // }

            //driver.FindElement(By.LinkText("Automation Tester")).Click();






            //10. Click on submit button.

            //  IWebElement submitbutton = driver.FindElement(By.XPath("//div[@class='btn btn-info']/input[@id='submit']"));
            // submitbutton.Click();
           
            try
            {
                Assert.IsTrue(driver.FindElement(By.Id("submit")).Displayed);
                Console.Write("Button Assertion Failed");
            }
            catch(Exception e) 
            {
                Console.Write(e);
            }
            Console.Read();
            // var element = driver.FindElement(By.XPath("/html/body/div[1]/div[3]/div/div[2]/main/div/div[1]/div/article/div/div/div[3]/div/div/div[29]/div[8]/div/button"));
            //element.Submit();
            driver.Quit();
        }



    }
}
